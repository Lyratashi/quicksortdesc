/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quicksortdesc;

/**
 *
 * @author Lz
 */
public class QuickSortDesc {
    
   public static void main(String args[]){TestEntiers.test();}
    static void permuter (int []t, int i,int j)
    {
	int temp = t[i];
        t[i] = t[j];
        t[j] = temp;
    }

    static void quickSortDesc (  int [] t, int indDeb,   int indFin)
    {
	if (indDeb < indFin-1){
            int indPivot = indDeb+((indFin-indDeb)/2);
            // position initiale du pivot = milieu du tableau
            int pivot = t[indPivot];
            int i =  indDeb, j = indFin;
            do {
                // Trouver l'indice du 1er element, de indDeb a indPivot, qui soit
                // superieur ou egal au pivot
                while(t[i] > pivot) ++i;
                // Trouver l'indice du premier element, de indFin a IndPivot ,
                // qui soit <= pivot
                while(t[j] < pivot)--j;
                // Permuter et mettre les indices à jour
                if (i<j) {
                    permuter(t, i,j );
                    if  (i == indPivot){ indPivot = j; ++i;}
                    else if (j == indPivot) { indPivot = i; --j;}
                    else { ++i; --j;} 
                }
            }while(i!=j);
            //Appel récursif sur le sous-tableau de gauche
            if (indDeb < indPivot-1) quickSortDesc(t, indDeb, indPivot-1);
            //Appel récursif sur le sous-tableau de droite
            if (indPivot < indFin-1) quickSortDesc(t, indPivot + 1, indFin);
        }
    }
    
}
